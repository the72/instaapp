//
//  ProfileInfoHeaderCollectionReusableView.swift
//  InstaApp
//
//  Created by Nurba on 25.02.2021.
//

import UIKit

protocol ProfileInfoHeaderCollectionReusableViewDelegate : AnyObject {
    func profileHeaderPostButton(_ header:ProfileInfoHeaderCollectionReusableView)
    func profileHeaderFollowingButton(_ header:ProfileInfoHeaderCollectionReusableView)
    func profileHeaderFollowersButton(_ header:ProfileInfoHeaderCollectionReusableView)
    func profileHeaderEditButton(_ header:ProfileInfoHeaderCollectionReusableView)
}

final class ProfileInfoHeaderCollectionReusableView: UICollectionReusableView {
    
        static let identifier = "ProfileInfoHeaderCollectionReusableView"
    
    public weak var delegate: ProfileInfoHeaderCollectionReusableViewDelegate?
    
    private let profilePhotoView:UIImageView = {
        let imageView = UIImageView()
    
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(systemName: "person")
        return imageView
    }()
    
    private let postsButton:UIButton = {
        let button = UIButton()
        button.setTitle("Posts", for: .normal)
        button.backgroundColor = .secondarySystemBackground
        button.setTitleColor(.label, for: .normal)
        return button
    }()
    
    private let followingButton:UIButton = {
        let button = UIButton()
        button.setTitle("Following", for: .normal)
        button.backgroundColor = .secondarySystemBackground
        button.setTitleColor(.label, for: .normal)
        return button
    }()
    
    private let followersButton:UIButton = {
        let button = UIButton()
        button.setTitle("Followers", for: .normal)
        button.backgroundColor = .secondarySystemBackground
        button.setTitleColor(.label, for: .normal)
        return button
    }()
    
    private let editProfileButton:UIButton = {
        let button = UIButton()
        button.setTitle("Edit your profile", for: .normal)
        button.backgroundColor = .secondarySystemBackground
        button.setTitleColor(.label, for: .normal)
        return button
    }()
    
    private let nameLabel:UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.text = "Nurba"
        label.numberOfLines = 1
        return label
    }()
    
    private let bioLabel:UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.text = "This is my first account"
        label.numberOfLines = 0
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .systemBackground
        clipsToBounds = true
        addSubViews()
        addButtonAction()
    }
    
    private func addSubViews(){
        addSubview(profilePhotoView)
        addSubview(postsButton)
        addSubview(followingButton)
        addSubview(followersButton)
        addSubview(nameLabel)
        addSubview(bioLabel)
        addSubview(editProfileButton)
    }
    
    private func addButtonAction(){
        followersButton.addTarget(self, action: #selector(didTapFollowerButton), for: .touchUpInside)
        followingButton.addTarget(self, action: #selector(didTapFollowingButton), for: .touchUpInside)
        postsButton.addTarget(self, action: #selector(didTapPostButton), for: .touchUpInside)
        editProfileButton.addTarget(self, action: #selector(didTapEditButton), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let profilePhotoSize = width/4
        profilePhotoView.frame = CGRect(x: 5,
                                        y: 5,
                                        width: profilePhotoSize,
                                        height: profilePhotoSize).integral
        
        profilePhotoView.layer.cornerRadius = profilePhotoSize/2.0
        
        let buttonHeight = profilePhotoSize/2
        let countButtonWidth = (width-10-profilePhotoSize)/3
        
        postsButton.frame = CGRect(x: profilePhotoView.right,
                                        y: 5,
                                        width: countButtonWidth,
                                        height: buttonHeight).integral
        followersButton.frame = CGRect(x: postsButton.right,
                                        y: 5,
                                        width: countButtonWidth,
                                        height: buttonHeight).integral
        followingButton.frame = CGRect(x: followersButton.right,
                                        y: 5,
                                        width: countButtonWidth,
                                        height: buttonHeight).integral
        editProfileButton.frame = CGRect(x: profilePhotoView.right,
                                        y: 5 + buttonHeight,
                                        width: countButtonWidth*3,
                                        height: buttonHeight).integral
        nameLabel.frame = CGRect(x: 5,
                                 y: 5 + profilePhotoView.bottom,
                                        width: width-10,
                                        height: 50).integral
        let bioSize = bioLabel.sizeThatFits(frame.size)
        bioLabel.frame = CGRect(x: 5,
                                 y: 5 + nameLabel.bottom,
                                        width: width-10,
                                        height: bioSize.height).integral
    }
    
    
    
    @objc private func didTapFollowerButton(){
        delegate?.profileHeaderFollowersButton(self)
    }
    
    @objc private func didTapFollowingButton(){
        delegate?.profileHeaderFollowingButton(self)
    }
    
    @objc private func didTapPostButton(){
        delegate?.profileHeaderPostButton(self)
    }
    
    @objc private func didTapEditButton(){
        delegate?.profileHeaderEditButton(self)
    }
}

//
//  StorageManager.swift
//  InstaApp
//
//  Created by Nurba on 24.02.2021.
//

import FirebaseStorage


public class StorageManager{
    static let share = StorageManager()
    
    private var bucket = Storage.storage().reference()
    
    public enum StorageErrorManager:Error{
        case failedToDownload
    }
    
    public func uploadUserPost(model: UserPost, completion:@escaping(Result<URL,Error>)-> Void){
        
    }
    
    public func downloadImage(with reference:String, completion:@escaping (Result<URL,StorageErrorManager>)->Void){
        bucket.child(reference).downloadURL(completion: {url, error in
            guard let url = url, error == nil else{
                completion(.failure(.failedToDownload))
                return
            }
            completion(.success(url))
        })
    }
    
}

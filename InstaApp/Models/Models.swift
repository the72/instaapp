//
//  Models.swift
//  InstaApp
//
//  Created by Nurba on 25.02.2021.
//

import Foundation


public enum UserPostType{
    case photo,video
}

public struct UserPost {
    let identifier: String
    let postType: UserPostType
    let thumbnailImage: URL
    let postURL: URL
    let caption: String?
    let likeCount: [PostLikes]
    let comments: [PostComment]
    let createdDate: Data
    let taggedUser:[String]
}

struct PostComment {
    let identifier: String
    let username:String
    let text:String
    let createdData: Data
    let likes: [CommentLikes]
    
}
struct PostLikes {
    let username:String
    let postIdentifier: String
}

struct CommentLikes {
    let username:String
    let commentIdentifier: String
}
enum Gender{
    case male,female
}
struct User {
    let username:String
    let bio:String
    let name:(first:String, last:String)
    let birthDate: Data
    let gender: Gender
    let counts:UserCount
    let joinDate: Data
}
struct UserCount {
    let follower: Int
    let following: Int
    let posts:Int
}
